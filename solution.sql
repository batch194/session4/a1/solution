-- S4 ACTIVITY

A) 
SELECT name AS Name FROM artists WHERE name LIKE "%d%" ORDER BY name ASC;

B) 
SELECT song_name AS Song, length as Length FROM songs WHERE length < 230 ORDER BY song_name ASC;

C) 
Select song_name AS Song, album_title AS Album, length as Length FROM albums 
	JOIN songs ON albums.id = songs.album_id ORDER BY song_name ASC;

D) 
SELECT name AS Name, album_title AS Album FROM artists
	JOIN albums ON artists.id = albums.artist_id WHERE album_title LIKE "%a%" ORDER BY name ASC;

E) 
Select * FROM albums ORDER BY album_title DESC LIMIT 4;

F) 
Select song_name AS Song, album_title AS Album FROM albums 
	JOIN songs ON albums.id = songs.album_id ORDER BY song_name ASC;